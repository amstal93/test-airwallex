packer {
  required_plugins {
    googlecompute = {
      version = ">= 1.0"
      source  = "github.com/hashicorp/googlecompute"
    }
    docker = {
      version = ">= 1.0.1"
      source  = "github.com/hashicorp/docker"
    }
  }
}

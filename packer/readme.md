## Packer Introduction

In this directory is the Packer configuration for the creation of safe images, using a Hardering script from [dockersec](https://github.com/TedLeRoy/docksec/blob/master/docksec.sh)

---
## Cloud Requirements

The items below are required prior to executing these codes.
- Service Account with premises to manage the following resources in Cloud:
  - Google Compute Engine
  - Compute Network
  - Cloud Storage

- A project is needed, with an active billing account.
  - Fill a value for `gcp_project_id` with the Project ID

---
## Packer Plugins

| Name | Version |
|------|---------|
| <a name="googlecompute"></a> [googlecompute](github.com/hashicorp/googlecompute) | 1.0 |
| <a name="docker"></a> [docker](#github.com/hashicorp/docker) | 1.0.1 |

## Packer Sources

| Name | Source | Plugin |
|------|--------|---------|
| <a name="docker_image"></a> docker_image | nginx:1.21.3-alpine | docker |
| <a name="gce_image"></a> gce_image | ubuntu-2004-focal-v20210927 | googlecompute |

---
## Packet Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_credentials_json"></a> [gcp\_credentials\_json](#input\_gcp\_credentials\_json) | File for access to GCP Project | `string` | `"./key.json"` | no |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | GCP project id that will be used | `string` | `""` | yes |
| <a name="input_region"></a> [region](#input\_region) | GCP region, e.g. southamerica-east1 | `string` | `"europe-west4"` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | GCP zone, e.g. us-east1-b (which must be in gcp\_region) | `string` | `"europe-west4-c"` | no |

---
## Terraform Outputs

No outputs.


# Configure a GCE instance group manager for the NGINX load balancer
resource "google_compute_instance_group_manager" "nginx" {
  name               = "ngx-instance-group"
  description        = "Instance group to host NGINX instances"
  base_instance_name = "nginx-instance-group"
  instance_template  = google_compute_instance_template.lb.self_link
  zone               = var.region_zone
  target_pools = [
    "${google_compute_target_pool.default.self_link}",
  ]
  target_size = 2
  auto_healing_policies {
    health_check      = google_compute_http_health_check.default.self_link
    initial_delay_sec = 300
  }
}

# Create a Google autoscaler for the LB instance group manager
resource "google_compute_autoscaler" "nginx" {
  name   = "nginx-autoscaler"
  zone   = var.region_zone
  target = google_compute_instance_group_manager.nginx.self_link
  autoscaling_policy {
    max_replicas = 5
    min_replicas = 2
    cpu_utilization {
      target = 0.5
    }
  }
}

# Create a Google compute instance template for the NGINX Plus load balancer
resource "google_compute_instance_template" "nginx" {
  name         = "ngx-instance-template"
  description  = "NGINX instance template"
  machine_type = var.machine_type
  tags = [
    "nginx-http-fw-rule",
  ]
  disk {
    source_image = "nginx-harder"
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
  service_account {
    scopes = [
      "https://www.googleapis.com/auth/compute",
    ]
  }
  metadata = {
    startup-script  = <<EOF
#!/bin/bash
echo "
services:
    nginx:
        depends_on:
            - reverseproxy
        image: nginx:alpine
        ports:
            - 80:80
        restart: always
" >>~/docker-compose.yml

docker-compose up -d -f ~/docker-compose.yml
EOF
    shutdown-script = <<EOF
#!/bin/bash

docker-compose down -f ~/docker-compose.yml
EOF

  }
  lifecycle {
    create_before_destroy = true
  }
}

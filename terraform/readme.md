## Terraform Introduction
In this folder are the terraform codes responsible for setting up an environment in Google Cloud.

---
## Cloud Requirements

The items below are required prior to executing these codes.
- Service Account with premises to manage the following resources in Cloud:
  - Google Compute Engine
  - Compute Network
  - Cloud Storage

- A project is needed, with an active billing account.
  - Fill a value for `gcp_project_id` with the Project ID

---

## Terraform Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 3.82 |
| <a name="requirement_google-beta"></a> [google-beta](#requirement\_google-beta) | ~> 3.82 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.1 |

## Terraform Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | 3.87.0 |
| <a name="provider_google-beta"></a> [google-beta](#provider\_google-beta) | 3.87.0 |

## Terraform Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_lb-https"></a> [lb-https](#module\_lb-https) | terraform-google-modules/terraform-google-lb-http/tree/master/modules/dynamic_backends | ~> 6.1 |
| <a name="module_network"></a> [network](#module\_network) | terraform-google-modules/network/google | ~> 3.4 |

## Terraform Resources

| Name | Type |
|------|------|
| [google_compute_autoscaler.nginx](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_autoscaler) | resource |
| [google_compute_instance_group_manager.nginx](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance_group_manager) | resource |
| [google_compute_instance_template.nginx](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance_template) | resource |
| [google_iap_brand.iap_brand](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iap_brand) | resource |
| [google_iap_client.iap_client](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iap_client) | resource |
| [google_project_service.apis](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_service) | resource |
| [google-beta_google_project.project](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/data-sources/google_project) | data source |
| [google_client_config.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |

## Terraform Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_domain"></a> [domain](#input\_domain) | A domain to set in Load Balancer HTTPS | `string` | `"teste.com"` | no |
| <a name="input_env"></a> [env](#input\_env) | Type of environment [test or prod] | `string` | `"test"` | no |
| <a name="input_gcp_credentials_json"></a> [gcp\_credentials\_json](#input\_gcp\_credentials\_json) | File for access to GCP Project | `string` | `"./key.json"` | no |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | GCP project id that will be used | `string` | `""` | yes |
| <a name="input_gcp_project_name"></a> [gcp\_project\_name](#input\_gcp\_project\_name) | GCP project name that will be used | `string` | `"devops"` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The type of virtual machine used to deploy NGINX | `string` | `"f1-micro"` | no |
| <a name="input_region"></a> [region](#input\_region) | GCP region, e.g. southamerica-east1 | `string` | `"europe-west4"` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | GCP zone, e.g. us-east1-b (which must be in gcp\_region) | `string` | `"europe-west4-c"` | no |

## Terraform Outputs

No outputs.

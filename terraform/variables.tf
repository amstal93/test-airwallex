# ##########################
# ### GENERAL  VARIABLES ###
# ##########################


################################
#### Project & General Configs

variable "gcp_project_id" {
  type        = string
  description = "GCP project id that will be used"
}

variable "gcp_project_name" {
  type        = string
  description = "GCP project name that will be used"
  default     = "devops"
}

variable "region" {
  type        = string
  description = "GCP region, e.g. southamerica-east1"
  default     = "europe-west4"
}

variable "zone" {
  type        = string
  description = "GCP zone, e.g. us-east1-b (which must be in gcp_region)"
  default     = "europe-west4-c"
}

variable "gcp_credentials_json" {
  type        = string
  description = "File for access to GCP Project"
  default     = "./key.json"
}

## -> GCP APIs to enable services.
### Ref. https://github.com/terraform-google-modules/terraform-google-sql-db#enable-apis

locals {
  apis_gcp = toset([
    "compute.googleapis.com",
    "sourcerepo.googleapis.com",
    "servicenetworking.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "vpcaccess.googleapis.com",
    "iap.googleapis.com",
    "servicemanagement.googleapis.com",
    "storage-api.googleapis.com",
  ])
}

variable "machine_type" {
  description = "The type of virtual machine used to deploy NGINX"
  default     = "f1-micro"
}

variable "domain" {
  type        = string
  description = "A domain to set in Load Balancer HTTPS"
  default     = "teste.com"
}